public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Animal animal1 = new Animal("Pui");
        Animal animal2 = new Animal("Quanh");
        System.out.println(animal1 + "," + animal2);

        Mammal mammal1 = new Mammal("Milu");
        Mammal mammal2 = new Mammal("Milo");
        System.out.println(mammal1 + "," + mammal2);

        Cat cat1 = new Cat("Bi");
        Cat cat2 = new Cat("Bo");
        System.out.println(cat1 + "," + cat2);

        Dog dog1 = new Dog("Sữa");
        Dog dog2 = new Dog("Cafe");
        System.out.println(dog1 + "," + dog1);

        System.out.println("Greets cat1:");
        cat1.greets();
        System.out.println("Greets cat2:");
        cat2.greets();

        System.out.println("Greets dog1:");
        dog1.greets();
        System.out.println("Greets dog2:");
        dog2.greets();

        System.out.println("Greets dog1 with dog2:");
        dog1.greets(dog2);
    }
}